﻿using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Gateway;
using Google.Protobuf;
using Grpc.Core;
using Grpc.Net.Client;
using Microsoft.AspNetCore.WebUtilities;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace streaming_asr_client_dotnet.com.apptek
{
    class Program
    {
        private const string LicenseServerUrl = "https://license.apptek.com";
        private const string TestFile = "sample1.wav";

        private readonly HttpClient _httpClient = new HttpClient();

        private LicenseResponse _licenseResponse;
        private GrpcChannel _channel;

        static async Task Main(string[] args)
        {
            if (args.Length < 1)
            {
                await Console.Error.WriteLineAsync("Usage:");
                await Console.Error.WriteLineAsync("dotnet Program \"<license-key>\"");
                Environment.Exit(1);
            }

            var program = new Program();

            await program.Init(args[0]);
            await program.CheckAvailabilityAsync();
            await program.ProcessFileAsync(TestFile);

            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }

        private async Task ProcessFileAsync(string testFile)
        {
            var client = new Gateway.Gateway.GatewayClient(_channel);

            var streamingCall = client.RecognizeStream();

            var readerTask = Task.Run(async () =>
            {
                await foreach (var result in streamingCall.ResponseStream.ReadAllAsync())
                {
                    if (result.Transcription != null && result.Transcription.Segment.Complete)
                        Console.WriteLine(result.Transcription.Orth);
                }
            });

            await streamingCall.RequestStream.WriteAsync(new RecognizeStreamRequest
            {
                Configuration = new RecognizeStreamConfig
                {
                    AudioConfiguration = new RecognizeAudioConfig
                    {
                        Encoding = RecognizeAudioEncoding.Pcm16BitSiMono,
                        SampleRateHz = 8000,
                        LangCode = "en-US"
                    },
                    LicenseToken = _licenseResponse.Token
                }
            });

            await using var fileStream = File.OpenRead(TestFile);
            var buffer = new byte[1024];
            int rad;
            while ((rad = await fileStream.ReadAsync(buffer)) > 0)
                await streamingCall.RequestStream.WriteAsync(new RecognizeStreamRequest
                {
                    Audio = ByteString.CopyFrom(buffer, 0, rad)
                });
            await streamingCall.RequestStream.CompleteAsync();
            await readerTask;
        }

        private async Task CheckAvailabilityAsync()
        {
            var client = new Gateway.Gateway.GatewayClient(_channel);
            using var call = client.RecognizeGetAvailableAsync(new AvailableRequest
                { LicenseToken = _licenseResponse.Token });
            var response = await call.ResponseAsync;
            var result = response.List.Select(m => m.ToString()).Aggregate((a, b) => a + "," + b);
            Console.WriteLine(result);
        }

        private async Task Init(string license)
        {
            var jwt = await GetTokenAsync(license);
            _licenseResponse = TokenToResponse(jwt);
            _channel = GrpcChannel.ForAddress("https://"
                                              + _licenseResponse.Payload.Host
                                              + ":"
                                              + _licenseResponse.Payload.Port);
        }

        private static LicenseResponse TokenToResponse(string token)
        {
            var chunks = token.Split(".");
                
            var payloadString = Encoding.UTF8.GetString(Base64UrlTextEncoder.Decode(chunks[1]));

            var payload = JsonConvert.DeserializeObject<LicenseResponse.TokenPayload>(payloadString,
                new JsonSerializerSettings
                {
                    ContractResolver = new DefaultContractResolver
                    {
                        NamingStrategy = new SnakeCaseNamingStrategy()
                    }
                });

            return new LicenseResponse(token, payload);
        }

        private async Task<string> GetTokenAsync(string license)
        {
            using var request =
                new HttpRequestMessage(HttpMethod.Get, LicenseServerUrl + $"/license/v2/token/{license}");
            using var response = await _httpClient.SendAsync(request, HttpCompletionOption.ResponseHeadersRead);
            return await response.Content.ReadAsStringAsync();
        }
    }
}