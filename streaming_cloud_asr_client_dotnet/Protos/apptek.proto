syntax = "proto3";

package gateway;
option go_package="gateway_grpc/gateway";

import "google/protobuf/wrappers.proto";

/*
* The AppTek gRPC gateway provides access to our streaming ASR service for over 30 languages and dialects.
* Our API provides apart from the realtime transcription of an audio stream also postprocessing (adding punctuation, capitalisation and translation of spoken forms of numbers, dates, spelled words, etc. into an easy to read and standard written form for most languages) and direct translation.
* Each request to the gateway requires a license token. See https://license.apptek.com/doc/index.html for details on how to acquire license tokens from the AppTek license server.
*/
service Gateway {
    /*
    * RecognizeGetAvailable returns all available languages for the RecognizeStream API.
    */
    rpc RecognizeGetAvailable(AvailableRequest) returns (RecognizeAvailableResponse) {}

    /*
    * RecognizeStream transcribes an incoming audio stream and returns the transcription in raw and postprocessed form and, if requested, translation into a target language.
    * The first and only the first message in the upstream must contain a RecognizeStreamConfig message, followed by messages containing audio data.
    * The audio data is automatically partitioned into segments, where each result message contains a segment id (starting with 1). Segment end events trigger a separate notification message.
    * During a segment returned results a subject to change. The last message in a segment is marked as "complete" and the next messages will be for the next segment.
    * The results for transcriptions, postprocessing and translations are independent of each other in the sense, that if e. g. the transcriptions already moved on to the next segments, trailing postprocessing and translation results can still be returned from the previous segment.
    * Not every transcription message triggers a corrensponding postprocessing and translation message, however it is guaranteed to always receive postprocessing and translation results for the "complete" segment transcription.
    * During the audio stream the API periodically returns progress status messages.
    */
    rpc RecognizeStream(stream RecognizeStreamRequest) returns (stream RecognizeStreamResponse) {}
}

/*
* AvailableRequest must the authenticated by the license token
*/
message AvailableRequest {
    string license_token = 1; // your license token
}

/*
* Direction indicates the display direction of a language
*/
enum Direction {
    Left2Right = 0; // left to right like in roman or germanic languages
    Right2Left = 1; // right to left like in arabic
}

/*
* Language description in detail
*/
message Language {
    string lang_code = 1; // language code
    string english_name = 2; // english name of the language
    string native_name = 3; // native namve of the language
    Direction direction = 4; // direction
}

/*
* RecognizeAvailable describes one available recognition engine
*/
message RecognizeAvailable {
    string domain = 1; // domain name
    uint32 sample_rate_hz = 2; // audio sample rate in Hz
    Language language = 3; // language details
    repeated Language mt_target_languages = 4; // list of available direct translations
}

/*
* RecognizeAvailableResponse contains the list of all available recognition engines.
* It is returned by the RecognizeGetAvailable API.
*/
message RecognizeAvailableResponse {
    repeated RecognizeAvailable list = 1;
}

/*
* RecognizeAudioEncoding lists all available audio encodings for the upstream
*/
enum RecognizeAudioEncoding {
    PCM_16bit_SI_MONO = 0; // PCM 16bit signed integer mono
}

/*
* RecognizeAudioConfig is the configuration message for the audio data in the RecognizeStream API.
* It must be send as the first and only the first message in the upstream.
*/
message RecognizeAudioConfig {
    RecognizeAudioEncoding encoding = 1; // encoding of the audio data
    uint32 sample_rate_hz = 2; // sample rate of the audio data in Hz
    string lang_code = 3; // language code of the audio
    string domain = 4; // domain name (leave empty if not sure)
    google.protobuf.UInt32Value mutable_suffix_length = 5; // maximum mutable suffix length (leave empty if not sure)
}

/*
* RecognizeTranslateInterval specifies the interval in which translation results are produced if direct translations of transcriptions is requested.
*/
enum RecognizeTranslateInterval {
    TRANSLATION_INTERVAL_SEGMENT_END = 0; // one translation only at segment end
    TRANSLATION_INTERVAL_CONTINUOUS = 1; // update translation multiple times during a segment.
}

/*
* RecognizeTranslateConfig is the optional translation configuration for the RecognizeStream API.
* Please make sure the requested target lang code is in the list of available mt target languages for your audio source language.
*/
message RecognizeTranslateConfig {
    RecognizeTranslateInterval translate_interval = 1; // interval in which translation results are produced
    string target_lang_code = 2; // lang code of the requested target language
}

/*
* RecognizeStreamConfig is the configuration message in the RecognizeStream API.
* It must be send as the first and only the first message in the upstream.
*/
message RecognizeStreamConfig {
    string license_token = 1; // your license token
    RecognizeAudioConfig audio_configuration = 2; // audio data configuration
    RecognizeTranslateConfig translate_configuration = 3; // optional translate configuration
}

/*
* RecognizeStreamRequest represents the upstream data in the RecognizeStream API.
* The first message must contain the configuration, all subsequent messages must contain audio data.
*/
message RecognizeStreamRequest {
    oneof oneof_request {
        RecognizeStreamConfig configuration = 1; // configuration message
        bytes audio = 2; // audio data
    }
}

/*
* RecognizeSegmentDetails contains the segment id (rolling number starting with 1) and a complete indicator. It is send with most result messages.
* If complete is set to true, this message will be the last message for the particular response type for the given segment id.
*/
message RecognizeSegmentDetails {
    uint32 id = 1; // segment id
    bool complete = 2; // true if and only if this is the last message for this response type with the given id, false otherwise
}

/*
* RecognizeWord describes all the details to a word in a RecognizeTranscription message
*/
message RecognizeWord {
    string word = 1; // recognized word
    uint64 start_time_ms = 2; // start time in milliseconds
    uint64 stop_time_ms = 3; // stop time in milliseconds
    float confidence = 4; // confidence score (between 0.0 and 1.0)
}

/*
* RecognizeTranscription is a partial recognition result.
*/
message RecognizeTranscription {
    RecognizeSegmentDetails segment = 1; // segment details
    string orth = 2; // the transcribed segment in one string
    repeated RecognizeWord words = 3; // list of all transcribed words with detailed information
}

/*
* RecognizeTranslation contains translation results.
*/
message RecognizeTranslation {
    RecognizeSegmentDetails segment = 1; // segment details
    string source = 2; // the source text for this translation
    string translation = 3; // the translated text
}

/*
* RecognizePostprocessing contains post-processed versions of recognized segments.
* For most languages we provide automatic postprocessing of raw recognition results in order to add punctuation, capitalisation and translation of spoken forms of numbers, dates, spelled words, etc. into an easy to read and standard written form.
* If postprocessing is not available for the source language, the postprocessed field matches the orth field.
* Not all RecognizeTranscription messages trigger a corrensponding RecognizePostprocessing message, but it is guaranteed to get the postprocessing result of the complete segment transcription.
*/
message RecognizePostprocessing {
    RecognizeSegmentDetails segment = 1; //segment details
    string orth = 2; // the source text
    string postprocessed = 3; // the postprocessed version of the source text
}

/*
* RecognizeProgressStatus messages are send from time to time to indicate the progress of the current stream.
* This is particular useful when segments in the audio do not contain speech, which might seem to make the recognizer unresponsive since there is nothing to return.
* Usually audio streams are limited in time. The remaining time value returns the remaining seconds of audio data the client is allowed to stream.
* All values are in seconds.
*/
message RecognizeProgressStatus {
    float audio_decoder_time_sec = 1; // audio decoder progress time in seconds
    float segmenter_progress_time_sec = 2; // segmenter progress time in seconds
    float recognizer_progress_time_sec = 3; // recognizer progress time in seconds
    float remaining_time_sec = 4; // remaining time in seconds
}

/*
* RecognizeSegmentEnd messages are send when the segmenter detects a segment end.
* These messages are helpful when the client intends to send a certain number of segments. Once the client receives the appropriate RecognizeSegmentEnd message, it can stop sending audio data.
*/
message RecognizeSegmentEnd {
    uint32 segment_id = 1; // segment id
    float segmenter_progress_time_sec = 2; // progress time in seconds since the last segment end in seconds
}

/*
* RecognizeStreamResponse represents the downstream data in the RecognizeStream API.
* It contains always one and only one response type.
*/
message RecognizeStreamResponse {
    oneof oneof_response {
        RecognizeTranscription transcription = 1; // a transcription response
        RecognizeSegmentEnd segment_end = 2; // a segment end response
        RecognizeProgressStatus progress_status = 3; // a progress status response
        RecognizePostprocessing postprocessing = 4; // a postprocessing response
        RecognizeTranslation translation = 5; // a translation response
    }
}