﻿namespace streaming_asr_client_dotnet.com.apptek
{
    public class LicenseResponse
    {
        public string Token { get; }
        public TokenPayload Payload { get; }

        public LicenseResponse(string token, TokenPayload payload)
        {
            Token = token;
            Payload = payload;
        }

        public class TokenPayload
        {
            // Undocumented
            public string AccountEmail { get; set; }
            public string AccountNumber { get; set; }
            public string ApiKey { get; set; }
            public string LogText { get; set; }

            public string SaveData { get; set; }

            // Documented
            public long Exp { get; set; }
            public string Host { get; set; }
            public long Iat { get; set; }
            public string Iss { get; set; }
            public string Jti { get; set; }
            public long MaxChannels { get; set; }
            public long MaxMinutes { get; set; }
            public int Port { get; set; }
            public string Task { get; set; }
        }
    }
}